<?php

namespace designerei\ContaoNavTogglerBundle\ContaoManager;

use designerei\ContaoNavTogglerBundle\ContaoNavTogglerBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoNavTogglerBundle::class)
                ->setLoadAfter([ContaoCoreBundle::class]),
        ];
    }
}
