<?php

$GLOBALS['TL_DCA']['tl_module']['palettes']['navToggler'] =
    '{title_legend},name,type;'
    . '{template_legend:hide},customTpl;'
    . '{protected_legend:hide},protected;'
    . '{expert_legend:hide},guests,cssID'
;

$GLOBALS['TL_DCA']['tl_module']['config']['onload_callback'][] = function ($dc) {
  $objFmd = Contao\ModuleModel::findByPk($dc->id);
  if ($objFmd->type === 'navToggler') {
    \Contao\Message::addInfo(sprintf($GLOBALS['TL_LANG']['tl_module']['includeTemplate'], 'j_navToggler'));
  }
};
